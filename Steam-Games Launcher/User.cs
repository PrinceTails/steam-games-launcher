﻿using System.Collections.Generic;

namespace Steam_Games_Launcher
{
    public class User
    {
        public int id;
        public string name;
        public List<int> gameList;
        public User()
        {
            id = 0;
            name = "";
            gameList = null;
        }
        public User(int i, string n, List<int> gl)
        {
            id = i;
            name = n;
            gameList = gl;
        }
    }
}
