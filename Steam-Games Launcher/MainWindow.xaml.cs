﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Diagnostics;
using System.IO;
using System.Threading;
namespace Steam_Games_Launcher
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Steam> Applications;
        bool isShortcut;
        bool addShortcuts;
        bool startupWindows;
        bool showInTray;
        bool aUpdate;
        double top_window;

        static ParameterizedThreadStart pts = new ParameterizedThreadStart(DeleteIcons);
        Thread delIcons = new Thread(pts);
        
        System.Windows.Forms.NotifyIcon tray;
        System.Windows.Forms.ContextMenu for_tray;
        System.Windows.Forms.MenuItem[] for_tray_item;
        System.Windows.Forms.MenuItem[] for_tray_games;
        System.Windows.Forms.Timer update_timer;

        public MainWindow()
        {
            InitializeComponent();
            gamespanel.SizeChanged += gamespanel_SizeChanged;
            try
            {
                import_settings();
                get_Steam_apps();
                createTrayMenu();
            }
            catch (Exception e)
            {
                StreamWriter nm = new StreamWriter("error.log", true);
                nm.WriteLine(DateTime.Now);
                nm.WriteLine(e.Message);
                nm.WriteLine(e.StackTrace);
                nm.WriteLine(e.Data);
                nm.Close();
            }
        }

        private void import_settings()
        {
            showInTray = Steam_Games_Launcher.Properties.Settings.Default.showInTray;
            addShortcuts = Steam_Games_Launcher.Properties.Settings.Default.shortcutsGames;
            startupWindows = Steam_Games_Launcher.Properties.Settings.Default.lauchWin;
            aUpdate = Steam_Games_Launcher.Properties.Settings.Default.autoUpdate;

            inTray.IsChecked = showInTray;
            shortG.IsChecked = addShortcuts;
            autoUpdate.IsChecked = aUpdate;
            winStartup.IsChecked = startupWindows;
        }

        public void createTrayMenu()
        {
            tray = new System.Windows.Forms.NotifyIcon();
            tray.Icon = Steam_Games_Launcher.Properties.Resources.post_42_1089322988;
            tray.Visible = false;
            tray.Text = "SteamGameLauncher";
            this.tray.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(tray_MouseDoubleClick);
            for_tray = new System.Windows.Forms.ContextMenu();
            for_tray_item = new System.Windows.Forms.MenuItem[4];
            tray.ContextMenu = for_tray;
            for_tray_item[1] = new System.Windows.Forms.MenuItem();
            for_tray_item[0] = new System.Windows.Forms.MenuItem();
            for_tray_item[2] = new System.Windows.Forms.MenuItem();
            for_tray_item[3] = new System.Windows.Forms.MenuItem();

            for_tray_item[3].Index = 0;
            for_tray_item[3].Text = "Выйти";
            for_tray_item[3].Click += new System.EventHandler(item_click_exit);
            for_tray_item[2].Index = 1;
            for_tray_item[2].Text = "Отображать в трее";
            for_tray_item[2].Checked = true;
            for_tray_item[2].Click += new System.EventHandler(item_click_showintray);
            for_tray_item[1].Index = 2;
            for_tray_item[1].Text = "Открыть окно программы";
            for_tray_item[1].Click += new System.EventHandler(item_click_showprogram);
            for_tray_item[0].Text = "Игры";

            for_tray_games = solve_games_in_tray();

            for_tray_item[0].MenuItems.AddRange(for_tray_games);
            for_tray.MenuItems.AddRange(for_tray_item);
        }

        private void tray_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Minimized)
            {
                tray.Visible = false;
                this.ShowInTaskbar = true;
                this.Show();
                this.WindowState = System.Windows.WindowState.Normal;
            }

        }
        private void item_click_exit(object sender, EventArgs e)
        {
            this.Close();
        }
        private void item_click_showintray(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Minimized)
            {
                tray.Visible = false;
                this.ShowInTaskbar = true;
                this.Show();
                this.WindowState = System.Windows.WindowState.Normal;
                for_tray_item[2].Checked = false;
                inTray.IsChecked = false;
                Steam_Games_Launcher.Properties.Settings.Default.showInTray = false;
            }
        }
        private void item_click_showprogram(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Minimized)
            {
                tray.Visible = false;
                this.ShowInTaskbar = true;
                this.Show();
                this.WindowState = System.Windows.WindowState.Normal;
            }
        }

        //создание и добавление кнопки на форму
        public void add_button(int index)
        {
            try
            {
                Button btn = new Button();
                System.Windows.Controls.Image img = new System.Windows.Controls.Image();
                img.Source = Applications[index].appImage;

                btn.TabIndex = index + 1;
                btn.Name = "Button_" + (index + 1);
                btn.Background = this.Background;
                btn.Content = img;
                btn.Opacity = 0.9;
                btn.Width = 64;
                btn.Height = 64;
                btn.ToolTip = Applications[index].appName;
                btn.BorderBrush = this.Background;
                btn.Foreground = this.Background;
                btn.Click += new RoutedEventHandler(start_app);
                gamespanel.Children.Add(btn);
            }
            catch (Exception e)
            {
                StreamWriter nm = new StreamWriter("error.log", true);
                nm.WriteLine(DateTime.Now);
                nm.WriteLine(e.Message);
                nm.WriteLine(e.StackTrace);
                nm.WriteLine(e.Data);
                nm.Close();
            }
        }

        //получение приложений Steam и создание кнопок
        public void get_Steam_apps()
        {
            try
            {
                Applications = new Steam().getApps(addShortcuts);
                Applications = new Steam().ArraySort(Applications);

                for (int i = 0; i < Applications.Count; i++)
                {
                    add_button(i);
                }
                if (aUpdate)
                {
                    update_timer = new System.Windows.Forms.Timer();
                    update_timer.Interval = 10000;
                    update_timer.Tick += Update_timer_Tick;
                    update_timer.Start();
                }

            }
            catch (Exception e)
            {
                StreamWriter nm = new StreamWriter("error.log", true);
                nm.WriteLine(DateTime.Now);
                nm.WriteLine(e.Message);
                nm.WriteLine(e.StackTrace);
                nm.WriteLine(e.Data);
                nm.Close();
            }
        }



        //запуск приложения
        public void start_app(object sender, RoutedEventArgs e)
        {
            Button button_tmp = (Button)sender;
            int app = button_tmp.TabIndex - 1;
            StartApplication(app);
        }
        public void start_app_from_tray(object sender, EventArgs e)
        {
            System.Windows.Forms.MenuItem button_tmp = (System.Windows.Forms.MenuItem)sender;
            int app = button_tmp.Index;
            StartApplication(app);
        }
        private void StartApplication(int id)
        {
            if (Applications[id] != null)
            {
                Process pr = new Process();
                isShortcut = false;
                if (Applications[id].isShortcut)
                {
                    if (File.Exists(Applications[id].appPath))
                    {
                        pr.StartInfo.FileName = Applications[id].appPath;
                        pr.Start();
                    }
                    else
                        MessageBox.Show("Возможно игра удалена.", "Не удалось запустить", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (!isShortcut)
                {
                    pr.StartInfo.FileName = "steam://rungameid/" + Applications[id].appId;
                    pr.Start();
                }
            }
        }

        private void windma_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void be_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void bm_Click(object sender, RoutedEventArgs e)
        {
            if (for_tray_item[2].Checked == true)
            {
                this.ShowInTaskbar = false;
                tray.Visible = true;
                this.WindowState = System.Windows.WindowState.Minimized;
                this.Hide();
            }
            else
            {
                this.ShowInTaskbar = true;
                tray.Visible = false;
                this.WindowState = System.Windows.WindowState.Minimized;
            }
        }

        private System.Windows.Forms.MenuItem[] solve_games_in_tray()
        {
            System.Windows.Forms.MenuItem[] games = new System.Windows.Forms.MenuItem[Applications.Count];

            for (int i = 0; i < Applications.Count; i++)
            {
                games[i] = new System.Windows.Forms.MenuItem();
                games[i].Text = Applications[i].appName;
                games[i].Index = i;
                games[i].Click += new EventHandler(start_app_from_tray);
            }

            return games;
        }

        private void gamespanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Applications == null)
                return;
            this.Height = 95;
            if(this.IsLoaded)
                this.Top -= this.Top-top_window;
            int count = Applications.Count;
            int rows = 0;
            if (count / 4 >= 4)
            {
                rows = 3;
                this.Height += rows * 64 + 15;
                this.Top -= (rows * 64 + 15) / 2;
            }
            else if (count / 4 == 1 && count%4==0) { }
            else
            {
                rows = count / 4;
                this.Height += rows * 64;
                this.Top -= (rows * 64) / 2;
            }
        }

        private void inTray_Click(object sender, RoutedEventArgs e)
        {
            if (inTray.IsChecked)
                for_tray_item[2].Checked = true;
            else
                for_tray_item[2].Checked = false;
            Steam_Games_Launcher.Properties.Settings.Default.showInTray = inTray.IsChecked;

        }

        private void windma_Closed(object sender, EventArgs e)
        {
            Steam_Games_Launcher.Properties.Settings.Default.Save();
        }

        private void windma_Loaded(object sender, RoutedEventArgs e)
        {
            for_tray_item[2].Checked = showInTray;
            top_window = this.Top;
            delIcons.Start(Applications);
        }

        private void shortG_Click(object sender, RoutedEventArgs e)
        {
            if (Applications == null)
                return;
            gamespanel.Children.Clear();
            if(addShortcuts)
            {
                addShortcuts = false;
                Steam_Games_Launcher.Properties.Settings.Default.shortcutsGames = false;
                List<Steam> app = new List<Steam>();
                for (int i = 0; i < Applications.Count; i++)
                {
                    if(!Applications[i].isShortcut)
                    {
                        app.Add(Applications[i]);
                    }
                }
                Applications = app;
            }
            else
            {
                addShortcuts = true;
                Steam_Games_Launcher.Properties.Settings.Default.shortcutsGames = true;
                new Steam().get_shortcuts(ref Applications);
                Applications = new Steam().ArraySort(Applications);
            }
            for (int i = 0; i < Applications.Count; i++)
                add_button(i);
            for_tray.MenuItems.Clear();
            createTrayMenu();
        }

        private void autoUpdate_Click(object sender, RoutedEventArgs e)
        {
            if(aUpdate)
            {
                aUpdate = false;
                Steam_Games_Launcher.Properties.Settings.Default.autoUpdate = false;
                autoUpdate.IsChecked = false;
                update_timer.Stop();
            }
            else
            {
                aUpdate = true;
                Steam_Games_Launcher.Properties.Settings.Default.autoUpdate = true;
                autoUpdate.IsChecked = true;
                if (update_timer != null)
                    update_timer.Start();
                else
                {
                    update_timer = new System.Windows.Forms.Timer();
                    update_timer.Interval = 10000;
                    update_timer.Tick += Update_timer_Tick;
                    update_timer.Start();
                }
            }
        }

        private void Update_timer_Tick(object sender, EventArgs e)
        {
            if (showInTray && this.WindowState == WindowState.Minimized)
            {
                tray.Visible = false;
                update_panel();
                tray.Visible = true;

            }
            else
                update_panel();
            
        }

        private bool equals_steam(List<Steam> src, List<Steam> dst)
        {
            if (dst == null)
                return true;
            if (src == null)
                return false;

            if (src.Count != dst.Count)
                return false;
            for(int i=0;i<src.Count; i++)
            {
                if (src[i].appId!=dst[i].appId || src[i].appImage.Decoder!=dst[i].appImage.Decoder)
                    return false;
            }
            return true;
        }

        private void update_panel()
        {
            List<Steam> tmp = new Steam().getApps(addShortcuts);
            tmp = new Steam().ArraySort(tmp);
            if (equals_steam(tmp, Applications))
                return;
            else
            {
                gamespanel.Children.Clear();
                Applications = tmp;
                Applications = new Steam().ArraySort(Applications);
                for (int i = 0; i < Applications.Count; i++)
                    add_button(i);

                for_tray.MenuItems.Clear();
                createTrayMenu();
            }
        }

        private void br_Click(object sender, RoutedEventArgs e)
        {
            update_panel();
            this.Focus();
        }

        private void winStartup_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.RegistryKey myKey =
                Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\", true);
            if (winStartup.IsChecked)
            {
                myKey.SetValue("Steam-Games Launcher", System.Reflection.Assembly.GetExecutingAssembly().Location);
                Steam_Games_Launcher.Properties.Settings.Default.lauchWin = true;
            }
            else
            {
                myKey.DeleteValue("Steam-Games Launcher");
                Steam_Games_Launcher.Properties.Settings.Default.lauchWin = false;
            }
        }

        static void DeleteIcons(object app)
        {
            List<Steam> applic = (List<Steam>)app;
            string iconPath = applic[0].iconPath;
            string [] fi = Directory.GetFiles(iconPath);
            for (int i=0;i<fi.GetLength(0);i++)
            {
                for(int j=0;j<applic.Count;j++)
                {
                    if (Path.GetFileNameWithoutExtension(fi[i]) == applic[j].hash)
                        break;
                    else if (j == applic.Count - 1)
                        File.Delete(fi[i]);
                }
            }
        }
    }
}
