﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;
using System.Diagnostics;
using System.Windows;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Net.NetworkInformation;

namespace Steam_Games_Launcher
{
    public class Steam
    {
        public int appId;
        public string appName;
        public BitmapFrame appImage;
        public string appPath;
        public bool isShortcut;
        private string os_path;
        public string iconPath;
        public string hash;
        //private List<User> u_list;
        //public List<User> owners;

        public Steam()
        {
            appId = 0;
            appName = "";
            appPath = "";
            isShortcut = false;
            iconPath = "";
            hash = "";
            OS_info();


        }
        //Для приложений Steam
        public Steam(int id, string name, BitmapFrame img, string h, string iconp)
        {
            appId = id;
            appName = name;
            appImage = img;
            //owners = owns;
            hash = h;
            iconPath = iconp;
        }

        public override string ToString()
        {
            return appName;
        }

        //Для сторонних приложений
        public Steam(string path, string name, BitmapFrame img)
        {
            appPath = path;
            appName = name;
            appImage = img;
            isShortcut = true;
            //owners = owns;
        }

        //получить список приложений
        public List<Steam> getApps(bool shortcuts)
        {
            //u_list = get_owners();

            List<Steam> Applications;

            List<FileInfo> appList = getList();
            GetIconsDirectory();
            
            Applications = ArraySteam(appList);

            if(shortcuts)
                get_shortcuts(ref Applications);
            return Applications;
        }

        //получение всех библиотек пользователя
        private string[] get_libraryfolders()
        {
            string[] lf;
            char[] split_char = { '"', '}', '{', '\n', '\t' };
            StreamReader stream = new StreamReader(os_path + @"\steamapps\libraryfolders.vdf");
            string text = stream.ReadToEnd();
            lf = text.Split(split_char, StringSplitOptions.RemoveEmptyEntries);
            lf[4] = os_path;
            return lf;
        }

        //получение списка индексов приложений
        private List<FileInfo> getList()
        {
            List<FileInfo> manifest_list = new List<FileInfo>();

            string[] lf = get_libraryfolders();
            for (int j = 4; j < lf.GetLength(0); j += 2)
            {

                DirectoryInfo dir = new DirectoryInfo(lf[j] + @"\SteamApps");

                FileInfo[] finfo = dir.GetFiles();
                for (int i = 0; i < finfo.GetLength(0); i++)
                {
                    string ch = Path.GetFileNameWithoutExtension(finfo[i].FullName);
                    if (ch.Contains("appmanifest"))
                        manifest_list.Add(finfo[i]);
                }
            }
            return manifest_list;
        }

        //static string ProgramFilesx86()
        //{
        //    if (8 == IntPtr.Size
        //        || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
        //    {
        //        return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
        //    }
        //    return Environment.GetEnvironmentVariable("ProgramFiles");
        //}

        //создание списка приложений-Steam

        private void OS_info()
        {
            if (Environment.Is64BitOperatingSystem)
                os_path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Steam";
            else
                os_path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Steam";
        }

        private List<Steam> ArraySteam(List<FileInfo> manifest_list)
        {
            List<Steam> app = new List<Steam>();
            char[] split_char = { '"', '}', '{', '\n', '\t' };

            for (int i = 0; i < manifest_list.Count; i++)
            {
                int id=0;
                string name="";

                byte[] buffer = new byte[128];

                //открытие и считывания данных из манифеста
                FileStream fs = manifest_list[i].OpenRead();
                fs.Read(buffer, 0, 128);
                fs.Close();

                string manifest_descriptor = Encoding.UTF8.GetString(buffer);
                string [] headers = manifest_descriptor.Split(split_char,StringSplitOptions.RemoveEmptyEntries);

                for(int j=0;j<headers.Length;j++)
                {
                    if (id==0 && headers[j] == "appid")
                    {
                        id = Convert.ToInt32(headers[j + 1]);
                        j++;
                        continue;
                    }
                    if (name == "" && headers[j] == "name")
                    {
                        name = headers[j + 1];
                        break;
                    }
                    if (j == headers.Length - 1 && name == "")
                        name = "Unknow Game";
                }


                BitmapFrame img;
                hash = CalculateHash(name + id.ToString());
                string iconpath = HasIcon(hash);
                if(iconpath!="")
                {
                    img = GetIcon(iconpath);
                }
                else if (HasInternetConnection())
                {
                    img = DownloadIcon(id, name);
                    if (img == null)
                    {
                        img = BitmapFrame.Create(new Uri("pack://application:,,/Images/no_icon.png"));
                    }
                }
                else
                {
                    img = BitmapFrame.Create(new Uri("pack://application:,,/Images/no_icon.png"));
                }

                app.Add(new Steam(id, name, img,hash,iconPath));
            }
            app = ArraySort(app);
            return app;
        }

        //сортировка списка игр по названию
        public List<Steam> ArraySort(List<Steam> list)
        {
            List<Steam> sort = new List<Steam>();

            List<string> names = new List<string>();

            for (int i = 0; i < list.Count; i++)
            {
                names.Add(list[i].ToString());
            }
            names.Sort();

            for (int i = 0; i < list.Count; i++)
                for (int j = 0; j < list.Count; j++)
                {
                    if (names[i] == list[j].ToString())
                        sort.Add(list[j]);
                }
            return sort;
        }

        //запущено ли приложение
        public bool IsLaunched()
        {
            Process[] procList = Process.GetProcesses();
            for (int i = 0; i < procList.GetLength(0); i++)
                if (procList[i].MainWindowTitle == appName)
                {
                    return true;
                }
            return false;
        }

        private BitmapFrame DownloadIcon(int id, string name)
        {
            int cur = 0;
            int readed = 0;
            string resp = "";
            string webSite = String.Format(@"http://steamdb.info/app/{0}/info", id);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(webSite);
            request.UserAgent = "Steam-Games Launcher by Prince Tails";
            request.Accept = "text/html";
            HttpWebResponse wRes = (HttpWebResponse)request.GetResponse();
            var da = wRes.GetResponseStream();

            do
            {
                byte[] buf = new byte[1024];
                readed = da.Read(buf, 0, 1024);
                cur += readed;
                resp += Encoding.UTF8.GetString(buf, 0, buf.Length);
            }
            while (cur <= 40000);
            wRes.Close();
            string regex = "href\\s*=\\s*(?:[\"'](?<1>[^\"']*)[\"']|(?<1>\\S+))";
            MatchCollection match = Regex.Matches(resp, regex);
            foreach (Match m in match)
            {
                string link = m.Groups[1].Value;
                if (link.Contains("https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/"))
                {
                    string istrue = link.Substring(link.Length - 3);
                    if (istrue == "ico")
                    {
                        BitmapFrame loadedBitmap = null;
                        
                        using (WebClient client = new WebClient())
                        {
                            try {
                                client.DownloadFile(link, String.Format(iconPath + @"\{0}.ico", hash));
                                client.Dispose();
                            }
                            catch
                            {

                            }
                        }
                        //считывать иконки с файла после загрузки
                        loadedBitmap = GetIcon(HasIcon(hash));
                        return loadedBitmap;
                    }
                }
            }
            return null;
        }

        //вычисление хеш-функции для формирования имени файла иконки
        private string CalculateHash(string name)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(name);

            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        //проверка доступа компьютера в интернет
        private bool HasInternetConnection()
        {
            try
            {
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions();

                options.DontFragment = true;

                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 120;
                PingReply reply = pingSender.Send("8.8.8.8", timeout, buffer, options);
                if (reply.Status == IPStatus.Success)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        //получение директории хранения иконок
        private void GetIconsDirectory()
        {
            string sp = System.IO.Path.GetDirectoryName(
      System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase.Substring(8));
            iconPath = Path.Combine(sp, "Icons");
            if (!Directory.Exists(iconPath))
                Directory.CreateDirectory(iconPath);
        }

        //скачана ли иконка
        private string HasIcon(string hash)
        {
            string path = iconPath + @"\" + hash + ".ico";
            if (File.Exists(path))
                return path;
            else
                return "";
        }

        //получение скачанной иконки
        private BitmapFrame GetIcon(string path)
        {
            BitmapFrame icon = null;
            Uri uri = new Uri(path);
            
            BitmapDecoder frames = BitmapDecoder.Create(uri, BitmapCreateOptions.None, BitmapCacheOption.None);
            for (int j = 0; j < frames.Frames.Count; j++)
            {
                BitmapFrame tmp;
                if (frames.Frames[j].Height <= 64 && frames.Frames[j].Width <= 64)
                {
                    tmp = frames.Frames[j];
                    icon = tmp;
                    return icon;
                }
            }
            return icon;
        }

        //#region get_users
        //private List<User> get_owners()
        //{
        //    List<User> owners = new List<User>();

        //    DirectoryInfo dir = new DirectoryInfo(os_path + @"\userdata");
        //    char[] split_char = { '"', '}', '{', '\n', '\t' };
        //    var sa = dir.GetDirectories();
        //    foreach (var f in sa)
        //    {
        //        List<int> game_list = new List<int>();
        //        DirectoryInfo di = new DirectoryInfo(f.FullName);
        //        var dirs = di.GetDirectories();
        //        for(int x=0; x<dirs.GetLength(0);x++)
        //        {
        //            int tmp;
        //            if (int.TryParse(dirs[x].Name, out tmp))
        //                game_list.Add(tmp);
        //        }

        //        string config = f.FullName + @"\config\localconfig.vdf";
        //        StreamReader file = new StreamReader(config);

        //        char[] text = new char[1500];
        //        file.ReadBlock(text, 0, 1500);
        //        file.Close();
        //        string rxt = new string(text);
        //        var sda = rxt.Split(split_char, StringSplitOptions.RemoveEmptyEntries);
        //        for (int i = 0; i < sda.GetLength(0); i++)
        //        {
        //            if(sda[i]=="PersonaName")
        //            {
        //                owners.Add(new User(Convert.ToInt32(sda[i+2]),sda[i + 1],game_list));
        //                break;
        //            }
        //        }
        //    }
        //    return owners;
        //}
        //#endregion

        #region shortcuts apps
        public void get_shortcuts(ref List<Steam> shortcuts)
        {
            DirectoryInfo dir = new DirectoryInfo(os_path + @"\userdata");
            var sa = dir.GetDirectories();
            foreach (var f in sa)
            {
                string fileshortcuts = f.FullName + @"\config\shortcuts.vdf";
                if (!File.Exists(fileshortcuts))
                    continue;
                StreamReader file = new StreamReader(fileshortcuts);
                string text = file.ReadToEnd();
                file.Close();
                ScanShortcuts(text, ref shortcuts);
            }
        }
        private void ScanShortcuts(string file, ref List<Steam> shortcuts)
        {
            var nm = file.Split(new char[] { '\0', '"' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 2; i < nm.GetLength(0); i++)
            {
                if (nm[i].Contains("appname"))
                {
                    string name = nm[i + 1];
                    string path = nm[i + 3];
                    BitmapFrame frame;

                    if (!File.Exists(path))
                        continue;
                    System.Drawing.Bitmap bitmap = System.Drawing.Icon.ExtractAssociatedIcon(path).ToBitmap();
                    BitmapSource btmpsrc = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                                                                          bitmap.GetHbitmap(),
                                                                          IntPtr.Zero,
                                                                          Int32Rect.Empty,
                                                                          BitmapSizeOptions.FromEmptyOptions());
                    frame = BitmapFrame.Create(btmpsrc);

                    shortcuts.Add(new Steam(path, name, frame));
                }
            }
        }
        private BitmapImage Bitmap2BitmapImage(System.Drawing.Bitmap bitmap)
        {
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Position = 0;
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();

            return bi;
        }
        #endregion
    }
}
